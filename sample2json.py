#!/usr/bin/env python3


import json
import os
import re
from os.path import join
import argparse
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument("--fastq_dir", help="Required. the FULL path to the fastq folder")
args = parser.parse_args()

assert args.fastq_dir is not None, "please provide the path to the fastq folder"


## default dictionary is quite useful!
## FILES[mysample1]["tumor"]["R1"]  value: a list of fastqs

FILES = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))

## build the dictionary with full path for each fastq.gz file
for root, dirs, files in os.walk(args.fastq_dir):
	for file in files:
		if file.endswith("fastq"):
			full_path = join(root, file)
			m = re.search(r"([A-Za-z]{3,4})-([A-Z0-9a-z]{4})-([A-Z0-9]{4})", file)
			if m:
				project = m.group(1)
				sample = m.group(2)
				plate = m.group(3)
				full_sample = project + "-" + sample + "-" + plate
				FILES[full_sample]["tumor"]["R1"].append(full_path)
			else:
				print("sample {file} is not captured by the regular expression".format(file = file))
				
print()
print ("total {} unique samples will be processed".format(len(FILES.keys())))
print ("------------------------------------------")
for sample in FILES.keys():
	print ("{sample} has {n} marks".format(sample = sample, n = len(FILES[sample])))
print ("------------------------------------------")
print("check the samples.json file for fastqs belong to each sample")
print()

js = json.dumps(FILES, indent = 4, sort_keys=True)
open('samples.json', 'w').writelines(js)


