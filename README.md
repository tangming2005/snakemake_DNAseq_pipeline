**Still under active development, not to use for production**

* will add mutect and vardict caller
* sample2json.py needs to deal with single-end and paired-end fastqs and tumor samples without matching normal controls.
* add copy number/tumor purity analysis into pipeline by sequenza.

### work flow of the pipeline

![](./rule_diagram.png)


### How to distribute workflows

read [doc](https://snakemake.readthedocs.io/en/stable/snakefiles/deployment.html)

```bash
ssh shark.mdanderson.org

# start a screen session
screen

# make a folder, name it yourself, I named it workdir
mkdir /rsch2/genomic_med/krai/workdir/

cd /rsch2/genomic_med/krai/workdir/

git clone https://gitlab.com/railab/snakemake_DNAseq_pipeline

cd snakemake_DNAseq_pipeline

## go to shark branch
git checkout shark

## edit the config.yaml file as needed, e.g. set mouse or human for ref genome, p value cut off for peak calling, the number of reads you want to downsample to
nano config.yaml

## skip this if on Shark, samir has py351 set up for you. see below STEPS
conda create -n snakemake python=3 snakemake
source activate snakemake
```

## STEPS 

### on Shark

there is a python3 environment set up. just do

```bash
source activate py351
```


### create the sample.json file  by feeding a fastq folder. this folder should be a folder containing all the samples.

**TODO**:  

write a different script for single-end and paired-end fastqs.
it is also matters if the tumor sample has a normal matching control or not.

a json file:

```python

"sample_name":
{"normal":
    {"R1":["normal1_R1.fastq", "normal2_R1.fastq"],
    "R2":["normal1_R2.fastq", "normal2_R2.fastq"]},
  "tumor":
    {"R1":["tumor1_R1.fastq", "tumor_2_R1.fastq"],
    "R2":["tumor1_R2.fastq", "tumor_2_R2.fastq"] }
}
```

please use the **full path** for the folder that contains your fastq folders.

`python3 sample2json.py --fastq_dir /rsch2/genomic_med/krai/rawfastqs/`

check the file information in the json file:

```
less -S samples.json 
```

### dry run to test 

```bash
## dry run
snakemake -np
```

if no errors, preceed below.

### Using [DRMAA](https://www.drmaa.org/)

[job control through drmaa](http://drmaa-python.readthedocs.io/en/latest/tutorials.html#controlling-a-job)

DRMAA is only supported on `Shark`.

```bash
module load drmma
./pyflow-drmaa-ChIPseq.sh
```

Using `drmaa` can `control + c` to stop the current run.

Dependent jobs are submitted one by one, if some jobs failed, the pipeline will stop. Good for initital testing.

### submit all jobs to the cluster

```bash
./pyflow-ChIPseq.sh 
```

All jobs will be submitted to the cluster on queue.  This is useful if you know your jobs will succeed for most of them and the jobs are on queue to gain priority.

### job control

To kill all of your pending jobs you can use the command:

```bash
bkill ` bjobs -u krai |grep PEND |cut -f1 -d" "`
```

```
      bjobs -pl
       Display detailed information of all pending jobs of the invoker.

      bjobs -ps
       Display only pending and suspended jobs.

      bjobs -u all -a
       Display all jobs of all users.

      bjobs -d -q short -m apple -u mtang1
       Display all the recently finished jobs submitted by john to the
       queue short, and executed on the host apple.
```

### rerun some of the jobs

```bash

# specify the name of the rule, all files that associated with that rule will be rerun. e.g. rerun macs2 calling peaks rule,
./pyflow-DNAseq.sh -R freebayes

## rerun one sample, just specify the name of the target file

./pyflow-DNAseq.sh 08vcfanno/SKCM-M409-P011_tumor.vep.anno.vcf

##rerun only vcfanno rule
./pyflow-DNAseq.sh -f vcfanno

## check snakemake -h
## -R -f -F --until are useufl
```

### checking results after run finish

```bash

snakemake --summary | sort -k1,1 | less -S

# or detailed summary will give you the commands used to generated the output and what input is used
snakemake --detailed-summary | sort -k1,1 > snakemake_run_summary.txt
```


### clean the folders

I use echo to see what will be removed first, then you can remove all later.

```
find . -maxdepth 1 -type d -name "[0-9]*" | xargs echo rm -rf
```


### Snakemake does not trigger re-runs if I add additional input files. What can I do?

Snakemake has a kind of “lazy” policy about added input files if their modification date is older than that of the output files. One reason is that information what to do cannot be inferred just from the input and output files. You need additional information about the last run to be stored. Since behaviour would be inconsistent between cases where that information is available and where it is not, this functionality has been encoded as an extra switch. To trigger updates for jobs with changed input files, you can use the command line argument –list-input-changes in the following way:

```bash
snakemake -n -R `snakemake --list-input-changes`

```

### How do I trigger re-runs for rules with updated code or parameters?

```bash
snakemake -n -R `snakemake --list-params-changes`
```

and

```bash
$ snakemake -n -R `snakemake --list-code-changes`
```

### freebayes paramters

borrowed from bcbio https://github.com/chapmanb/bcbio-nextgen/blob/8c3206f131f9b97715d33141e558e1e38a07ac12/bcbio/variation/freebayes.py#L154

--min-repeat-entropy 1  
--no-partial-observations  

### GATK best practice

![](./GATK_best_practise_somatic.png)

### Note on read group

read this [post](http://gatkforums.broadinstitute.org/gatk/discussion/6472/read-groups) in the GATK forum.

example:  

```
@RG ID:H0164.2  PL:illumina PU:H0164ALXX140820.2    LB:Solexa-272222    PI:0    DT:2014-08-20T00:00:00-0400 SM:NA12878  CN:BI
```

Meaning of the read group fields required by GATK

    * ID = Read group identifier
    This tag identifies which read group each read belongs to, so each read group's ID must be unique. It is referenced both in the read group definition line in the file header (starting with @RG) and in the RG:Z tag for each read record. Note that some Picard tools have the ability to modify IDs when merging SAM files in order to avoid collisions. In Illumina data, read group IDs are composed using the flowcell + lane name and number, making them a globally unique identifier across all sequencing data in the world.
    Use for BQSR: ID is the lowest denominator that differentiates factors contributing to technical batch effects: therefore, a read group is effectively treated as a separate run of the instrument in data processing steps such as base quality score recalibration, since they are assumed to share the same error model.

    * PU = Platform Unit
    The PU holds three types of information, the {FLOWCELL_BARCODE}.{LANE}.{SAMPLE_BARCODE}. The {FLOWCELL_BARCODE} refers to the unique identifier for a particular flow cell. The {LANE} indicates the lane of the flow cell and the {SAMPLE_BARCODE} is a sample/library-specific identifier. Although the PU is not required by GATK but takes precedence over ID for base recalibration if it is present. In the example shown earlier, two read group fields, ID and PU, appropriately differentiate flow cell lane, marked by .2, a factor that contributes to batch effects.

    * SM = Sample
    The name of the sample sequenced in this read group. GATK tools treat all read groups with the same SM value as containing sequencing data for the same sample, and this is also the name that will be used for the sample column in the VCF file. Therefore it's critical that the SM field be specified correctly. When sequencing pools of samples, use a pool name instead of an individual sample name.

    * PL = Platform/technology used to produce the read
    This constitutes the only way to know what sequencing technology was used to generate the sequencing data. Valid values: ILLUMINA, SOLID, LS454, HELICOS and PACBIO.

    * LB = DNA preparation library identifier
    MarkDuplicates uses the LB field to determine which read groups might contain molecular duplicates, in case the same DNA library was sequenced on multiple lanes.

If your sample collection's BAM files lack required fields or do not differentiate pertinent factors within the fields, use Picard's AddOrReplaceReadGroups to add or appropriately rename the read group fields as outlined [here](http://gatkforums.broadinstitute.org/gatk/discussion/2909).

Use Picard's AddOrReplaceReadGroups to appropriately label read group (@RG) fields, coordinate sort and index a BAM file. Only the five required @RG fields are included in the command shown. Consider the other optional @RG fields for better record keeping.

```bash
java -jar picard.jar AddOrReplaceReadGroups \ 
    INPUT=reads.bam \ 
    OUTPUT=reads_addRG.bam \ 
    RGID=H0164.2 \ #be sure to change from default of 1
    RGLB= library1 \ 
    RGPL=illumina \ 
    RGPU=H0164ALXX140820.2 \ 
    RGSM=sample1 \ 
```

**Deriving ID and PU fields from read names**

```
H0164ALXX140820:2:1101:10003:23460
H0164ALXX140820:2:1101:15118:25288
```

Breaking down the common portion of the query names:

```
H0164____________ #portion of @RG ID and PU fields indicating Illumina flow cell
_____ALXX140820__ #portion of @RG PU field indicating barcode or index in a multiplexed run
_______________:2 #portion of @RG ID and PU fields indicating flow cell lane
```

**Multi-sample and multiplexed example**

Suppose I have a trio of samples: MOM, DAD, and KID. Each has two DNA libraries prepared, one with 400 bp inserts and another with 200 bp inserts. Each of these libraries is run on two lanes of an Illumina HiSeq, requiring 3 x 2 x 2 = 12 lanes of data. When the data come off the sequencer, I would create 12 bam files, with the following @RG fields in the header:

```
Dad's data:
@RG     ID:FLOWCELL1.LANE1      PL:ILLUMINA     LB:LIB-DAD-1 SM:DAD      PI:200
@RG     ID:FLOWCELL1.LANE2      PL:ILLUMINA     LB:LIB-DAD-1 SM:DAD      PI:200
@RG     ID:FLOWCELL1.LANE3      PL:ILLUMINA     LB:LIB-DAD-2 SM:DAD      PI:400
@RG     ID:FLOWCELL1.LANE4      PL:ILLUMINA     LB:LIB-DAD-2 SM:DAD      PI:400

Mom's data:
@RG     ID:FLOWCELL1.LANE5      PL:ILLUMINA     LB:LIB-MOM-1 SM:MOM      PI:200
@RG     ID:FLOWCELL1.LANE6      PL:ILLUMINA     LB:LIB-MOM-1 SM:MOM      PI:200
@RG     ID:FLOWCELL1.LANE7      PL:ILLUMINA     LB:LIB-MOM-2 SM:MOM      PI:400
@RG     ID:FLOWCELL1.LANE8      PL:ILLUMINA     LB:LIB-MOM-2 SM:MOM      PI:400

Kid's data:
@RG     ID:FLOWCELL2.LANE1      PL:ILLUMINA     LB:LIB-KID-1 SM:KID      PI:200
@RG     ID:FLOWCELL2.LANE2      PL:ILLUMINA     LB:LIB-KID-1 SM:KID      PI:200
@RG     ID:FLOWCELL2.LANE3      PL:ILLUMINA     LB:LIB-KID-2 SM:KID      PI:400
@RG     ID:FLOWCELL2.LANE4      PL:ILLUMINA     LB:LIB-KID-2 SM:KID      PI:400
```

### Install gemini and annotation data

Gemini is a tool from Aaron Quinlan's group

```bash
conda install -c bioconda gemini

cd /scratch/genomic_med/apps/gemini

gemini update --dataonly
```

The annotation data were downloaded to `/scratch/genomic_med/apps/gemini/gemini/data`


to annotate the vcf files, use `vcfanno`:
configuration file:  

https://github.com/brentp/vcfanno/blob/master/example/gem.conf

### filter freebayes calls

from bcbio http://bcb.io/2015/03/05/cancerval/

```bash
bcftools filter --soft-filter 'FBQualDepth' -e '(AF[0] <= 0.5 && (DP < 4 || (DP < 13 && %QUAL < 10))) || (AF[0] > 0.5 && (DP < 4 && %QUAL < 50))' -m '+' 

```

### Install VEP

The latest version of vep is on github
http://www.ensembl.org/info/docs/tools/vep/script/vep_download.html#installer

it is version 89 when this was written.(bioinformatics tools evolve too fast!)

check this gist as well https://gist.github.com/ckandoth/f265ea7c59a880e28b1e533a6e935697

```bash
cd /scratch/genomic_med/apps
git clone https://github.com/Ensembl/ensembl-vep.git
cd ensembl-vep
git status
# the Ensembl API will be installed
perl INSTALL.pl
```

```bash
export VEP_DATA="/scratch/genomic_med/apps/ensembl-vep-data"
export VEP_PATH="/scratch/genomic_med/apps/ensembl-vep"

rsync -avhP rsync://ftp.ensembl.org/ensembl/pub/release-89/variation/VEP/homo_sapiens_vep_89_GRCh37.tar.gz $VEP_DATA
tar -xvzf $VEP_DATA/homo_sapiens_vep_89_GRCh37.tar.gz -C $VEP_DATA
```
install the reference FASTAs for GRCh37:  

a fasta file `$VEP_DATA/homo_sapiens/89_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa.gz` will be downloaded.

```bash
perl INSTALL.pl --AUTO f --SPECIES homo_sapiens --ASSEMBLY GRCh37 --DESTDIR $VEP_PATH --CACHEDIR $VEP_DATA

```
Convert the offline cache for use with tabix, that significantly speeds up the lookup of known variants:

```bash
perl convert_cache.pl --species homo_sapiens --version 89_GRCh37 --dir $VEP_DATA
```

#### Annotate

```bash
vep --species homo_sapiens --assembly GRCh37 --offline --no_stats --sift b --ccds --uniprot --hgvs --symbol --numbers --domains --gene_phenotype --canonical --protein --biotype --uniprot --tsl --pubmed --variant_class --shift_hgvs 1 --check_existing --total_length --allele_number --no_escape --xref_refseq --failed 1 --vcf --minimal --flag_pick_allele --pick_order canonical,tsl,biotype,rank,ccds,length --dir $VEP_DATA --fasta $VEP_DATA/homo_sapiens/89_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa.gz --input_file example_GRCh37.vcf --output_file example_GRCh37.vep.vcf --polyphen b --af_1kg --af_esp --regulatory 
```

If you got an error message:

```
-------------------- EXCEPTION --------------------
MSG: ERROR: Cannot index bgzipped FASTA file with Bio::DB::Fasta
```
most likely, you perl version is too old.
see an issue [here](https://github.com/mskcc/vcf2maf/issues/97)

options:

> 1. You can update your systems perl the suggested version (which was not suitable in my case)  
> 2. Or one can install a local version of the correct perl version. see [here](https://gist.github.com/ckandoth/c16b9a423b54dc0a7a37)  
> 3. As the error is caused by reading a gziped file, one can simply unzip the reference.

Make sure you have write access to the folder where the fasta file resides. I am placing the fasta in our department shared folder.

from http://www.ensembl.org/info/docs/tools/vep/script/vep_options.html

>  The first time you run the script with this parameter an index will be built which can take a few minutes. This is required if fetching HGVS annotations (--hgvs) or checking reference sequences (--check_ref) in offline mode (--offline).

### Prioritize the tumor-only variants

from bcbio: http://bcb.io/2015/03/05/cancerval/

>then extracts these to prioritize variants with high or medium predicted impact, not present in 1000 genomes or ExAC at more than 1% in any subpopulation, or identified as pathenogenic in COSMIC or ClinVar. 