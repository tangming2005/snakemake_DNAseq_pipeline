shell.prefix("set -eo pipefail; echo BEGIN at $(date); ")
shell.suffix("; exitstat=$?; echo END at $(date); echo exit status was $exitstat; exit $exitstat")

configfile: "config.yaml"

localrules: all
# localrules will let the rule run locally rather than submitting to cluster
# computing nodes, this is for very small jobs


FILES = json.load(open(config['SAMPLES_JSON']))

CLUSTER = json.load(open(config['CLUSTER_JSON']))

SAMPLES = sorted(FILES.keys())

## list all samples by tumor and normal
ALL_SAMPLES = []
for sample in SAMPLES:
    for tn in FILES[sample].keys():
        ALL_SAMPLES.append(sample + "_" + tn)


CONTROLS = [sample for sample in ALL_SAMPLES if '_normal' in sample]
CASES = [sample for sample in ALL_SAMPLES if '_tumor' in sample]

## list BAM files
CONTROL_BAM = expand("03aln/{sample}.sorted.bam", sample=CONTROLS)
CASE_BAM = expand("03aln/{sample}.sorted.bam", sample=CASES)

ALL_BAM     = CONTROL_BAM + CASE_BAM
ALL_FASTQC  = expand("02fqc/{sample}.sorted_fastqc.zip", sample = ALL_SAMPLES)
ALL_INDEX = expand("03aln/{sample}.sorted.bam.bai", sample = ALL_SAMPLES)
ALL_FLAGSTAT = expand("03aln/{sample}.sorted.bam.flagstat", sample = ALL_SAMPLES)
ALL_QC = ["10multiQC/multiQC_log.html"]
ALL_FREEBAYES = expand("06freebayes/{sample}.vcf", sample = CASES)
ALL_VEP = expand("08vep/{sample}_anno_vep.vcf", sample = CASES)
ALL_VCFANNO = expand("07vcfanno/{sample}_anno.vcf", sample = CASES)
ALL_VEP_TABLE = expand("09vep2table/{sample}_vep.tsv", sample = CASES)

rule all:
    input: ALL_FASTQC + ALL_BAM + ALL_INDEX + ALL_FLAGSTAT + ALL_QC + ALL_FREEBAYES + ALL_VEP + ALL_VCFANNO + ALL_VEP_TABLE

def get_fastq(wildcards):
    sample = "_".join(wildcards.sample.split("_")[0:-1])
    tn = wildcards.sample.split("_")[-1]
    return FILES[sample]['tumor']['R1']


## the resulting file names are mysample_tumor.sorted.bam
## mysample_normal.sorted.bam
# wildcard {sample} will be mapped to mysample_tumor or mysample_normal
def get_input_files(wildcards):
    sample = "_".join(wildcards.sample.split("_")[0:-1])
    tn = wildcards.sample.split("_")[-1]
    if config["paired_end"]:
        return  FILES[sample][tn]['R1'] + FILES[sample][tn]['R2']
    ## single end sequencing
    if not config["paired_end"]:
        return FILES[sample][tn]['R1']


## deal with paired end and single end data
## see https://bitbucket.org/snakemake/snakemake/pull-requests/148/examples-for-paired-read-data-and/diff

rule align:
    input:  get_input_files
    output: "03aln/{sample}.sorted.bam", "03aln/{sample}.sorted.bam.bai", "00log/{sample}.align"
    threads: CLUSTER["align"]["cpu"]
    params:
            jobname = "{sample}",
            ## add read group for bwa mem mapping, change accordingly if you know PL:ILLUMINA, LB:library1 PI:200 etc...
            rg = "@RG\tID:{sample}\tSM:{sample}"
    message: "aligning bwa {input}: {threads} threads"
    log:
        bwa = "00log/{sample}.align",
        markdup = "00log/{sample}.markdup"
    run:
        ## paired end reads
        if config["paired_end"]:
            if config["long_reads"]:
                shell(
                    r"""
                    bwa mem -t 5 -M -v 1 -R '{params.rg}' {config[ref_fa]} {input[0]} {input[1]} 2> {log.bwa} \
                    | samblaster 2> {log.markdup} \
                    | samtools sort -m 2G -@ 5 -T {output[0]}.tmp -o {output[0]}
                    samtools index {output[0]}
                    """)
            ## short reads < 70bp
            ## Probably one of the most important is how many mismatches you will allow between a read and a potential mapping location for that location to be considered a match.
            ## The default is 4% of the read length, but you can set this to be either another proportion of the read length, or a fixed integer
            else:
                shell(
                    r"""
                    bwa aln -t 5 {config[ref_fa]} {input[0]} 2> {log.bwa} > 03aln/{wildcards.sample}_R1.sai
                    bwa aln -t 5 {config[ref_fa]} {input[1]} 2>> {log.bwa} > 03aln/{wildcards.sample}_R2.sai
                    bwa sampe -r '{params.rg}' {config[ref_fa]} 03aln/{wildcards.sample}_R1.sai 03aln/{wildcards.sample}_R2.sai {input[0]} {input[1]} 2>> {log.bwa} \
                    | samblaster 2> {log.markdup} \
                    | samtools sort -m 2G -@ 5 -T {output[0]}.tmp -o {output[0]}
                    rm 03aln/{wildcards.sample}_R1.sai 03aln/{wildcards.sample}_R2.sai
                    samtools index {output[0]}
                    """)

        # single end  reads
        else:
            if config["long_reads"]:
                shell(
                    r"""
                    bwa mem -t 5 -M -v 1 -R '{params.rg}' {config[ref_fa]} {input} 2> {log.bwa} \
                    | samblaster 2> {log.markdup} \
                    | samtools sort -m 2G -@ 5 -T {output[0]}.tmp -o {output[0]}
                    samtools index {output[0]}
                    """)
            ## short reads < 70bp
            else:
                shell(
                    r"""
                    bwa aln -t 5 {config[ref_fa]} {input} 2> {log.bwa} > 03aln/{wildcards.sample}.sai
                    bwa samse -r '{params.rg}' {config[ref_fa]} 03aln/{wildcards.sample}.sai {input} 2>> {log.bwa} \
                    | samblaster 2> {log.markdup} \
                    | samtools sort -m 2G -@ 5 -T {output[0]}.tmp -o {output[0]}
                    rm 03aln/{wildcards.sample}.sai
                    samtools index {output[0]}
                    """)

## there is some complication to get the fastqc result names to match the ALL_FASTQC target, fastqc uses the prefix of the .fastq files as the output
## I will use mapped bam files for fastqc (note no reads filtering should be done in the aligning step), as the bam file names have been standardized.

rule fastqc:
    input:  "03aln/{sample}.sorted.bam"
    output: "02fqc/{sample}.sorted_fastqc.zip", "02fqc/{sample}.sorted_fastqc.html"
    log:    "00log/{sample}_fastqc"
    threads: CLUSTER["fastqc"]["cpu"]
    params : jobname = "{sample}"
    message: "fastqc {input}: {threads}"
    shell:
        """
        module load fastqc
        fastqc -o 02fqc -f bam --noextract {input} 2> {log}
        """

rule flagstat_bam:
    input:  "03aln/{sample}.sorted.bam"
    output: "03aln/{sample}.sorted.bam.flagstat"
    log:    "00log/{sample}.flagstat_bam"
    threads: 1
    params: jobname = "{sample}"
    message: "flagstat_bam {input}: {threads} threads"
    shell:
        """
        samtools flagstat {input} > {output} 2> {log}
        """

rule multiQC:
    input :
        expand("00log/{sample}.align", sample = ALL_SAMPLES),
        expand("03aln/{sample}.sorted.bam.flagstat", sample = ALL_SAMPLES),
        expand("02fqc/{sample}.sorted_fastqc.zip", sample = ALL_SAMPLES)
    output: "10multiQC/multiQC_log.html"
    log: "00log/multiqc.log"
    message: "multiqc for all logs"
    shell:
        """
        multiqc 02fqc 03aln 00log -o 10multiQC -d -f -v -n multiQC_log 2> {log}

        """

# rule mutect:

## parameters borrowed from bcbio http://bcb.io/2015/03/05/cancerval/
## the naive support of mulit-thread may not be ideal, may change to freebayes --targets with pre-defined regions
## like what speedseq does https://github.com/hall-lab/speedseq/blob/master/bin/speedseq
rule freebayes:
    input: "03aln/{sample}.sorted.bam", "03aln/{sample}.sorted.bam.bai"
    output: "06freebayes/{sample}.vcf.tmp"
    log: "00log/{sample}_freebayes.log"
    params:
        jobname = "{sample}",
        outputdir = os.path.dirname(srcdir("00log"))
    threads: 12
    shell:
        """
        source activate root
        ## freebayes-parallel uses scripts in the ../vcflib/scripts and ../vcflib/bin, I have to cd into the directory first
        cd /scratch/genomic_med/apps/freebayes/freebayes/scripts/
        ./freebayes-parallel {config[regions_include]} 4 -f {config[ref_fa]} \
        --genotype-qualities \
        --ploidy 2 \
        --min-repeat-entropy 1 \
        --no-partial-observations \
        --report-genotype-likelihood-max \
        {params.outputdir}/{input[0]} 2> {params.outputdir}/{log} > {params.outputdir}/{output}
        """

rule clean_vcf:
    input: "06freebayes/{sample}.vcf.tmp"
    output: "06freebayes/{sample}.vcf"
    log: "00log/{sample}_clean_vcf.log"
    params:
        jobname = "{sample}"
    threads: 4
    shell:
        """
        cat {input} \
        | vcfallelicprimitives -t DECOMPOSED --keep-geno \
        | vcffixup - \
        | vcfsort \
        | vt decompose -s - \
        | vt normalize -n -r {config[ref_fa]} -q - \
        | vcfuniqalleles 2> {log} > {output}
        """

### annotate with vcfanno

rule vcfanno:
    input: "06freebayes/{sample}.vcf"
    output: "07vcfanno/{sample}_anno.vcf"
    log: "00log/{sample}_vcfanno.log"
    params:
        jobname = "{sample}"
    threads: 4
    shell:
        """
        vcfanno -p 4 -base-path {config[vcfanno_db]} -lua {config[vcfanno_lua]} {config[vcfanno_config]} {input} > {output} 2> {log}
        """

### annotate with VEP

rule vep:
    input: "07vcfanno/{sample}_anno.vcf"
    output: "08vep/{sample}_anno_vep.vcf"
    log: "00log/{sample}_vep.log"
    params:
        jobname = "{sample}"
    threads: 4
    shell:
        """
        # Try using --fork (see http://www.ensembl.org/info/docs/tools/vep/script/vep_options.html#forking)
        # Not only should this eradicate any memory leak issues, but you should see the script run much, much faster.
        # the parameters are from https://gist.github.com/ckandoth/f265ea7c59a880e28b1e533a6e935697
        vep --species homo_sapiens --assembly GRCh37 --offline --no_stats \
        --sift b --ccds --uniprot --hgvs --symbol --numbers --domains --gene_phenotype  \
        --canonical --protein --biotype --uniprot --tsl --pubmed --variant_class --shift_hgvs 1 \
        --check_existing --total_length --allele_number --no_escape --xref_refseq --failed 1 --vcf \
        --polyphen b --af_1kg --af_esp --regulatory \
        --minimal --flag_pick_allele --pick_order canonical,tsl,biotype,rank,ccds,length \
        --dir {config[VEP_DATA]} --fasta {config[VEP_fa]} \
        --fork 4 \
        --input_file {input} \
        --output_file {output} 2> {log}

        ## annotate with vep first, and then vcfanno later. example from gemini
        ## http://gemini.readthedocs.io/en/latest/content/functional_annotation.html

        """

rule vep2table:
    input: "08vep/{sample}_anno_vep.vcf"
    output: "09vep2table/{sample}_vep.tsv"
    log: "00log/{sample}_vep2table.log"
    params:
        jobname = "{sample}"
    threads: 1
    shell:
        """
        source activate root
        python ./scripts/convert_vep_vcf_to_tsv.py {input} > {output} 2> {log}
        """

### make a gemini database

## copy-number
